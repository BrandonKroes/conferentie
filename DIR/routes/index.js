var express = require('express');
var router = express.Router();
var _ = require('underscore');
var Timeslot = require('../models/timeslot.js');
var Agenda = require('../models/agenda.js');
var AgendaTimes = require('../models/agenda.json');

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Express' });
});
router.get('/loggedIndex', function (req, res) {
    res.render('loggedIndex');
});

router.get('/programma', function (req, res) {
    Agenda.getAgenda(function(err, result){
      if(err){
        console.log(err + "errr");
          res.render('message', { message_title: 'ERR', message: 'ERR' });
      }else{
        res.render('agenda', {Agenda : result});
      }

    });
});


router.get('/login', function (req, res) {
    res.render('login');
});

router.get('/registration', function (req, res) {
    res.render('registreren');
});


router.get('/slot', function (req, res) {
    res.render('slot');
});


router.get('/loggedIn', function (req, res) {
    res.render('LogMessage', { message_title: 'Ingelogd', message: 'U bent ingelogd!' });
});


module.exports = router;
