﻿'use strict'
var express = require('express');
var router = express.Router();
var config = require('../dbConn/config');
var _ = require('underscore');
var Timeslot = require('../models/timeslot.js');
var TimeslotValue = require('../models/timeslot.json');
var User = require('../models/user.js');

router.get('/', function (req, res) {
  if(  req.session.idRole == 2){
    Timeslot.Reviewable(function (err, result) {
        if (err) {
            console.log(err);
            res.render('message', { message_title: 'Error', message: 'Er heeft zich een error voor gedaan' });
        } else {
            console.log(TimeslotValue);

            res.render('slot', { timeslot: result.rows, timeslotDate: TimeslotValue });
        }
    });
}

});

router.get('/building', function(req,res){
    User.HowManyIn(function(err, result){
      if(err){
        console.log(err);
      }else{
        res.render('in', {amount : result});
      }

    })
});

router.get('/TimeslotAccept/:rid', function (req, res) {
    Timeslot.AcceptRequest(req.params.rid, function(err, result){
      if(err){
        console.log(err)
      }else{
        res.render('AdminMessage', {
            message_title: "Geaccepteerd!",
            message: "Tijdslot geaccepteerd!, de spreker krijgt er een email over"
        });


      }


    })

});
router.get('/TimeslotRefuse/:rid', function (req, res) {
Timeslot.getSlot(req.params.rid, function(err, slotdata){
  if(err){
    console.log(err);
  }else{

  Timeslot.DenyRequest(req.params.rid, function(err, result){
    if(err){
      console.log(err);
    }else{


      Timeslot.getEmailByTimeslot(req.params.rid, function(err, emailUser){
        if(err){
          console.log(err);
        }
        else{
          var helper = require('sendgrid').mail;

          var from_email = new helper.Email("noreply@conferentie.com");
          var to_email = new helper.Email(emailUser);
          var subject = "Gastspreker afgewezen";
          var content = new helper.Content("text/plain", "Helaas moet ik u mededelen dat uw aanvraag voor het zijn van een gastpreker is afgewezen" + JSON.stringify(slotdata));
          var mail = new helper.Mail(from_email, subject, to_email, content);


          var sg = require('sendgrid')(config.sengridAPI);
          var request = sg.emptyRequest({
              method: 'POST',
              path: '/v3/mail/send',
              body: mail.toJSON()
          });

          sg.API(request, function (error, response) {
              console.log(response.statusCode);
              console.log(response.body);
              console.log(response.headers);
          });
          res.render('AdminMessage', {
              message_title: "Afgewezen!",
              message: "Aanvraag Afgewezen!, de gebruiker krijgt er een email over"
          });
        }

      })
    }
  })
}
});
});

router.get('/TimeslotSA/:rid', function (req, res) {
  Timeslot.SecondaryChoice(req.params.rid, function(err, result){
    if(err){
      console.log(err);
    }else{
    res.render('AdminMessage', {
        message_title: "Geaccepteerd!",
        message: "Alternatief Tijdslot geaccepteerd!, de spreker krijgt er een email over"
    });
  }

});
});
module.exports = router;
