﻿'use strict'
var express = require('express');
var router = express.Router();
var config = require('../dbConn/config');
var _ = require('underscore');
var Order = require('../models/Order');

var Timeslot = require('../models/timeslot.js');

router.get('/succes', function (req, res) {
    var ido = req.session.idOrder;

    Order.approveOrder(ido, function (err, result) {
        if (err) {
            console.log(err);
            res.redirect('/order/failure');
        } else {
            res.render('message', { message_title: 'Sucess', message: 'De reservering is gelukt, u krijgt uw ticket nu naar u toegestuurd via de email' });
        }

    });
});

//Redirect if payment was unsuccesful
router.get('/failure', function (req, res) {
    res.render('message', { message_title: 'Mislukt', message: 'De reservering is mislukt, probeer het later nogmaals!' });
});


//Redirect to tickets page, but only if logged in

router.get('/tickets', function (req, res) {
    Timeslot.getAvailableTickets(function(err, result){
        if(err){
          console.log(err);
        }else{
          var ticketAmount = result.rows[0];
          console.log("tickets " + ticketAmount);
          res.render('tickets', {ticketAmount : ticketAmount});
        }
    })
    });


//Handler of temporary reservation data
router.post('/confirm', function (req, res) {
    //if (typeof (req.session.idUser) != "undefined" && req.session.idUser !== null) {


    let TOTALPRICE = 0;
    let ticketprice = 0;
    let lunchprice = 0;
    let dinerprice = 0;
    let totalfoodprice = 0;

    console.log(JSON.stringify(req.body));
    if(typeof(req.body.Parsepartout) !=  'undefined'){

          TOTALPRICE = 100;
          ticketprice = 100;
          var ticketArray = ["0", "1", "2"];


    }else{
      var ticketArray = _.union(req.body.ticket);
      //parse discount
      if (combi === '012') {
          TOTALPRICE = 100;
          ticketprice = 100;
      }
      //Saturday sunday discount
      else if (combi === '23') {
          ticketprice = 90;
          TOTALPRICE = 80;
      }

      //If there arent any discount options made, the TOTALPRICE will be registered based on ticket TOTALPRICE.
      else {
          for (let a = 0; a < ticketArray.length; a++) {
              console.log("Aan de beurt is dag + " + ticketArray[a]);
              switch (parseInt(ticketArray[a])) {
                  //TOTALPRICE ticket friday
                  case 0:
                      ticketprice += 45;
                      TOTALPRICE += 45;
                      break;
                  //TOTALPRICE ticket saturday
                  case 1:
                      ticketprice += 45;
                      TOTALPRICE += 60;
                      break;
                  //TOTALPRICE ticket sunday
                  case 2:
                      ticketprice += 30;
                      TOTALPRICE += 30;
                  default:
                      TOTALPRICE += 0;
              }
          }
          console.log(TOTALPRICE);
      }
    }
    let diner = _.union(req.body.diner);
    let lunch = _.union(req.body.lunch);



    //Calculation to see if there are any combinations that deserve a discount.
    var combi = ticketArray.toString().replace(/\,/g, "");




    //Calculating the TOTALPRICE of lunch
    for (let a = 0; a < lunch.length; a++) {

        lunchprice += 20;
    };

    //Calculating  the TOTALPRICE of diner
    for (let a = 0; a < diner.length; a++) {
        dinerprice += 30;
    }


    totalfoodprice = lunchprice + dinerprice;
    TOTALPRICE += totalfoodprice;

    console.log("de totale prijs is = " + TOTALPRICE);
    console.log("de totale prijs van tickets  is = " + ticketprice);
    console.log("de totale prijs van voedsel is = " + totalfoodprice);
    console.log("de totale prijs van lunch voedsel is = " + lunchprice);
    console.log("de totale prijs van diner voedsel is = " + dinerprice);
    console.log(JSON.stringify(ticketArray));
    //creating an object store all the information
    var order = {
        items: {
            tickets: ticketArray,
            diner: diner,
            lunch: lunch,
            meals: diner.concat(lunch)
        },
        price: {
            ticketprice: ticketprice,
            totalfoodprice: totalfoodprice,
            lunchprice: lunchprice,
            dinerprice: dinerprice,
            totalprice: TOTALPRICE
        },


        idUser: req.session.idUser

    }

    //saving order to session of user
    req.session.order = order;
    console.log(JSON.stringify(order));
    console.log(JSON.stringify(order["price"]["totalprice"]));
    console.log(JSON.stringify(order["idUser"]));
    console.log(JSON.stringify(order["items"]["diner"].length));

    res.render('revieworder', {
        ticketprice: ticketprice,
        lunchprice: lunchprice,
        dinerprice: dinerprice,
        totalprice: TOTALPRICE
    });


});



//Handler of finalized reservation
router.get('/reservationstatus', function (req, res) {
    console.log("aangekomen");
    //checking if the user is logged in, if not redirect to index
    if (typeof (req.session.idUser) != "undefined" && req.session.idUser !== null) {

        let obje = req.session.order;
        console.log("functie in");
        Order.insertNewOrder(obje, function (err, result) {
            if (err) {
                console.log(err);
                res.redirect('/order/failure');
            } else {
                console.log(result);
                if (err) {
                    res.redirect('/order/failure');
                }
                else {
                    req.session.idOrder = result;
                    res.render('reservationoutcome');
                }
            }
        });

    } else {
        res.render('index');
    }
});




module.exports = router;
