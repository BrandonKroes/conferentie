'use strict'
var express = require('express');
var router = express.Router();
var config = require('../dbConn/config');
var _ = require('underscore');
var User = require('../models/User');
var Tag = require('../models/tags.js')
var Timeslot = require('../models/timeslot.js');
var TimeslotValue = require('../models/timeslot.json');


/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});


router.get('/tickets', function (req, res) {
    Timeslot.getAvailableTickets(function(err, result){
        if(err){
          console.log(err);
        }else{
          var ticketAmount = result.rows[0];
          console.log("tickets " + ticketAmount);
          res.render('tickets', {ticketAmount : ticketAmount});
        }
    })
    });

router.post('/attemptlogin', function (req, res) {
    res.render('login');
});

router.post('/newRegistration', function (req, res) {
    var mail = req.body.email;
    var obj = {
        name: req.body.voornaam + req.body.achternaam,
        streetname: req.body.straat,
        city: req.body.stad,
        zipcode: req.body.postcode,
        country: req.body.land,
        province: req.body.provincie,
        password: req.body.password,
        email: req.body.email
    };
    User.checkExistingEmail(obj, function (err, result) {
        if (err) console.log(err);
        else {
            console.log('result ' + result);
            if (result === 0) {
                User.insertNewUser(obj, function (err, hash) {
                    if (err) console.log(err);
                    else {
                        console.log("link " + hash);
                        var helper = require('sendgrid').mail;


                        var from_email = new helper.Email("noreply@conferentie.com");
                        var to_email = new helper.Email(mail);
                        var subject = "Uw account voor conferentie ICT";
                        var content = new helper.Content("text/plain", "www.lde.com:9900/users/act/" + hash);
                        var mail = new helper.Mail(from_email, subject, to_email, content);


                        var sg = require('sendgrid')(config.sengridAPI);
                        var request = sg.emptyRequest({
                            method: 'POST',
                            path: '/v3/mail/send',
                            body: mail.toJSON()
                        });

                        sg.API(request, function (error, response) {
                            console.log(response.statusCode);
                            console.log(response.body);
                            console.log(response.headers);
                        });
                        res.render('message', { message_title: 'Controleer uw email!', message: 'Om uw account te activeren ga naar uw mail' });
                    }
                });
            } else {
                console.log("Email al bekend");
                console.log(result);
                console.log(err);
                res.render('message', { message_title: 'Error', message: 'Email staat al in het systeem.' });
            }
        }
    });
});



router.get('/cancel', function (req, res) {
    res.render('cancel');
});

router.get('/request', function (req, res) {

  if (typeof (req.session.idUser) != "undefined" && req.session.idUser !== null) {

    var AvailableTimes = TimeslotValue;
    var TakenTimes = new Array();
    Timeslot.OpenSlots(function (err, results) {
        if (err) {
            console.log(err);
            res.render('message', { message_title: 'Error', message: 'Er heeft zich een error voor gedaan' });
            var Openslots = results;
        } else {
            Timeslot.prefferedSlot(function(err, choicepref){
              if(err){
                console.log("===== Request PREFFERED ====" + err);
              }
              else{
                Tag.getTags(function(err, result){
                  if(err){
                    console.log("==== TAGS ===");
                  }else {

                    res.render('request', { Openslots: results, TakenTimes: choicepref, Tags : result });
                  }
                })

              }

                })

        }
    });
  }else{
    res.render('index');
  }

});

router.get('/act/:url', function (req, res) {
    var activationcode = req.params.url;
    console.log("route getriggered");
    User.activationURL(activationcode, function (err, result) {
        if (err) {
            console.log(err);
            res.render('message', { message_title: 'Error', message: 'Er heeft zich een error voor gedaan' });
        } else {
            res.render('message', { message_title: 'Account geactiveerd!', message: 'Uw account is geactiveerd u kan nu inloggen!' });
        }
    });
});

router.post('/filledrequest', function (req, res){
    console.log(JSON.stringify(req.body));
    if(req.body.prefferedslotchoice == "empty"){
      var prefslot = null;
    }else{
      var prefslot = req.body.prefferedslotchoice;
    }
    var userid = req.session.idUser;
    var obj = {

      timeslot: {
        idSlot : req.body.slotchoice,
        wishes : req.body.wishes,
        prefferedSlot : prefslot,
        topic : req.body.topic,
        description : req.body.description
      },
      tags : req.body.tags,
      idUser : userid

    }
    console.log(JSON.stringify(obj));
    Timeslot.Request(obj, function(err, result){
      if(err){
        console.log(err);
        res.render('LogMessage', { message_title: 'ERR', message: 'zoek hulp bij de organisator' });
      }else{
        console.log(result);
        res.render('LogMessage', { message_title: 'Aanvraag ingediend', message: 'Uw account is geactiveerd u kan nu inloggen!' });
      }
    });
});

router.post('/login', function (req, res) {

    console.log("hier");
    var obj = {
        email: req.body.email,
        password: req.body.password
    };

    console.log(obj);
    User.login(obj, function (err, result) {
        if (err) console.log(err);
        else {

            if (result === false) {


                res.render('message', { message_title: 'Inlog gegevens incorrect', message: 'probeer het nogmaals' });

            } else {

                req.session.idUser = result.rows[0].idUser;
                req.session.idRole = result.rows[0].idRole;

                if(req.session.idRole == 2){
                 res.render('AdminMessage', { message_title: 'Ingelogd', message: 'U bent ingelogd!' });
                }else{
                  res.render('LogMessage', { message_title: 'Ingelogd', message: 'U bent ingelogd!' });

                }

            }
        }
    });


});

router.get('/logout', function (req, res) {
    req.session.destroy();
    res.render('message', { message_title: 'Uitgelogd', message: 'U bent uitgelogd!' });

});



module.exports = router;
