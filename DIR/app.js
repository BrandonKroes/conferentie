var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var routes = require('./routes/index');
var users = require('./routes/users');
var order = require('./routes/order');
var admin = require('./routes/admin');
var memcached = require('connect-memcached')(session);
var cookieStore = new memcached({ host: "localhost:11211" });

var app = express();
const port = 9900 || process.env.PORT;

const config = require('./dbConn/config.js');
//DB CONNECT string
const conn = config.conn;

var sess = {
    secret: "supersecret",
    saveUninitialized: true,
    resave: true,
    store: cookieStore,
    cookie: { maxAge: 180000 } //enkele minuten == 180000
}


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session(sess));

app.use('/', routes);
app.use('/users', users);
app.use('/order', order);
app.use('/admin', admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(port, function () {
    console.log('----======<<< STARTED ON PORT: ' + port + ' >>>======----');
});

module.exports = app;
