﻿'use strict'

//The LTE is a local test enviromert where new functions are tested.

var Timeslot = require('../models/timeslot.js');
var TimeslotData = require('../models/timeslot.json');
var qr = require('qr-image');
const pg = require('pg')
const config = require('../dbConn/config.js');

var Client = require('pg').Client;

var conn = new Client(config.conn);
conn.connect();

var rollback = function (conn) {
    //terminating a conn connection will
    //automatically rollback any uncommitted transactions
    //so while it's not technically mandatory to call
    //ROLLBACK it is cleaner and more correct
    conn.query('ROLLBACK', function () {
        conn.end();
    });
};


var obj = {
    name: "testname",
    streetname: "straatnaam",
    city: "stad",
    zipcode: "postcode",
    country: "Country",
    province: "Province",
    password: "testwachtwoord",
    email: "test@mail.nl"
};



Timeslot.getEmailByTimeslot = function(idTimeslot, callback){

  conn.query('select "idUser" from "userTimeslot" where "idTimeslot" = $1;', [idTimeslot], function(err, result){
    if(err){
      console.log(err);
      return callback(err, null);
    }else{
      var idUser = result.rows[0].idUser;
      conn.query('select "email"  from "Login" where "idUser" = $1', [idUser], function(err, result){

        if(err){
          console.log(err);

        }else{
          console.log(result.rows[0].email);
          return callback(null, result.rows[0].email);
        }
      })
    }

  })
}
