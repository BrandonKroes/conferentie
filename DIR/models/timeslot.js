'use strict';
const pg = require('pg');
const config = require('../dbConn/config.js');
var sendgrid = require('sendgrid')(config.sendgridAPI);
var Client = require('pg').Client;
var _ = require('underscore');
var moment = require('moment');
var Tag = require('../models/tags.js')
var User = require('../models/user.js')
var qr = require('qr-image');
var pdfkit = require('pdfkit');
var fs = require('fs');

var conn = new Client(config.conn);
conn.connect();

var rollback = function (conn) {
    //terminating a conn connection will
    //automatically rollback any uncommitted transactions
    //so while it's not technically mandatory to call
    //ROLLBACK it is cleaner and more correct
    conn.query('ROLLBACK', function () {

    });
};

var Timeslot = function () {

};

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

Timeslot.getAvailableTickets = function(callback){
  conn.query('select * from "TicketSales";', function(err, result){
    if(err){
      return callback(err, null);
    }else {
      console.log(JSON.stringify(result));
      return callback(null, result);
    }

  })

}

Timeslot.Request = function (obj, callback) {


  conn.query('BEGIN', function (err, result) {
    if(err){
      console.log(err);
    }else{
      conn.query('INSERT INTO "Timeslot" ("idSlot", "topic", "wishes", "prefferedSlot", "description", "reviewed") VALUES ($1, $2, $3, $4, $5, FALSE) RETURNING "idTimeslot";', [ obj["timeslot"].idSlot, obj["timeslot"].topic, obj["timeslot"].wishes,  obj["timeslot"].prefferedSlot, obj["timeslot"].description], function (err, result) {
                 if(err){
                  console.log(err);
                  rollback(conn);
                  return callback(err, null);

                }else{
                  console.log("return hier " + JSON.stringify(result.rows[0].idTimeslot));
                  console.log(obj.idUser);
                  console.log(JSON.stringify(obj));
                  obj['timeslot'].idTimeslot = result.rows[0].idTimeslot;
                  Tag.TimeslotTag(obj, function(err, result){
                    if(err){
                      console.error(err);
                      rollback(conn);
                      return callback(err, null);
                    }else{
                      conn.query('INSERT INTO "userTimeslot" ("idTimeslot", "idUser") VALUES ($1, $2);', [obj['timeslot'].idTimeslot, obj.idUser], function(err, result){
                          if(err){
                            console.error(err);
                            rollback(conn);
                            return callback(err, null);
                          }

                      })


                    }
                  });

                }
              });
            }
          });

          conn.query('COMMIT');
          return callback(null, true);

}


Timeslot.getEmailByTimeslot = function(idTimeslot, callback){

  conn.query('select "idUser" from "userTimeslot" where "idTimeslot" = $1;', [idTimeslot], function(err, result){
    if(err){
      console.log(err);
      return callback(err, null);
    }else{
      console.log(result);
      var idUser = result.rows[0].idUser;
      conn.query('select "email"  from "Login" where "idUser" = $1', [idUser], function(err, result){

        if(err){
          console.log(err);

        }else{
          console.log(result.rows[0].email);
          return callback(null, result.rows[0].email);
        }
      })
    }

  })
}

Timeslot.Reviewable = function (callback) {
    conn.query('select * from "Timeslot" where "reviewed" = FALSE order by "idSlot" ASC;', function (err, result) {
        if (err) {
            console.log("====== Reviewable ==== " + err);
            return callback(err, null);
        } else {
            return callback(null, result);
        }

    });
};

//Function that returns all slot id's that are either taken or waiting a review
Timeslot.ChosenSlots = function (callback) {
    conn.query('SELECT * FROM "Timeslot" order by "idSlot" ASC;', function (err, result) {
        if (err) {
            console.log("====== Taken slots ==== " + err);
            return callback(err, null);
        } else {
            return callback(null, result);
        }
    });
}

//Function that returns all the slots that are taken.
Timeslot.ConfirmedSlots = function (callback) {
    conn.query('SELECT * FROM "Timeslot" where "reviewed" = TRUE order by "idSlot" ASC;', function (err, result) {
        if (err) {
            console.log("====== Confirmed Timeslot ==== " + err);
            return callback(err, null);
        } else {
            return callback(null, result);
        }
    });
}

Timeslot.Program = function (callback) {
    conn.query('SELECT * FROM "Timeslot" order by "idSlot" ASC;', function (err, result) {
        if (err) {
            console.log("=== Program ==== " + err);
            return callback(err, null);
        } else {
            return callback(null, result.rows);
        }

    });
}

Timeslot.prefferedSlot = function(callback){
  conn.query('Select "SlotTimes"."idSlot" , "slotValue" from "SlotTimes", "Timeslot" where "Timeslot"."reviewed" = False and  "Timeslot"."idSlot" = "SlotTimes"."idSlot" order by "idSlot" ASC;', function(err, result){
    if(err){
      console.log('=== PREFFERED SLOT === ' + err);
      return callback(err, null);
    }else {
      console.log(result.rows + " is de prefferedSlot uitkomst");
      return callback(null, result.rows);
    }
  });

}

Timeslot.AcceptRequest = function (idTimeslot, callback) {
    conn.query('update "Timeslot" SET "reviewed" = TRUE WHERE "idTimeslot" = $1 RETURNING "idSlot";', [idTimeslot], function (err, result) {
        if (err) {
            console.log("==== Accept Request ==== " + err);
            return callback(err, null);
        } else {

          console.log(JSON.stringify(result.rows[0].idSlot));
          Timeslot.GetUserIDbyTimeslot(idTimeslot, function(err, idUser){
            if(err){
              console.log(err);
            }else{
              Timeslot.ToDateConvert(parseInt(result.rows[0].idSlot), function(err, result){
                if(err){
                  console.log(err);
                }else{
                  console.log(JSON.stringify(result));
                  var obj = {
                    idUser : idUser,
                    ticket : parseInt(result)
                  }
                  Timeslot.generatePDFAndSendMail(obj, function(err, result){
                    if(err){
                      console.log("======================== PDF GEN ERR ==== " + err);
                    }else{
                      return callback(null, true);
                    }

                  });

                }

              })

            }


          });

        }

    });

}

Timeslot.generatePDFAndSendMail = function(obj, callback){
  var OrderData = new Array();
var QRdata = new Array();
  conn.query('BEGIN', function (err, result) {
      if (err) { console.log(err); rollback(conn); return callback(err, null); }
      //New Order

      conn.query('INSERT INTO "Order" ("idOrder", "orderDate", "orderPrice", "orderState", "orderPaid") VALUES ($1, $2, $3, TRUE, TRUE) RETURNING "idOrder";', [randomString(34, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), moment().format("lll"), 0], function (err, result) {
          if (err) {
              console.log(' ==== O40 ===' + err);
              rollback(conn); return callback;
          }
          var ido = result.rows[0].idOrder;

          function stringToAscii(s) {
              var ascii = "";
              if (s.length > 0)
                  for (var i = 0; i < s.length; i++) {
                      var c = "" + s.charCodeAt(i);
                      while (c.length < 3)
                          c = "0" + c;
                      ascii += c;
                  }
              return (ascii);
          }

          let q = stringToAscii(ido);
          var pdfName = q + ".pdf";
          //Connect order to a user
          conn.query('INSERT INTO "userOrders" ("idUser", "idOrder") VALUES ($1, $2);', [obj["idUser"], ido], function (err, result) {
              //disconnect after successful commit
              if (err) {
                  console.log(' ==== O48 ===' + err);
                  rollback(conn); return callback;
              }

                  //Create new ticket

                  conn.query('INSERT INTO "Ticket" ("ticketIn", "ticketDay", "barcode") VALUES (FALSE, $1, $2) RETURNING "idTicket";', [obj["ticket"], randomString(34, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')], function (err, result) {
                      //disconnect after successful commit
                      if (err) {
                          console.log(' ==== O106 ===' + err);
                          rollback(conn); return callback;
                      }
                      console.log("klaar met Tickets");
                      let idt = result.rows[0].idTicket;
                      var svg_string = qr.svgObject(idt, { type: 'svg' });
                      var qrdata = {
                          pdfstring: "Ticket voor Dag " + (obj["ticket"] + 1),
                          svg: svg_string.path
                      };
                      QRdata.push(qrdata);
                      //Assign ticket to an order
                      console.log("order nummer is " + idt);
                      conn.query('INSERT INTO "orderTickets" ("idTicket", "idOrder") VALUES ($1, $2);', [idt, ido], function (err, result) {
                          //disconnect after successful commit
                          if (err) {
                              rollback(conn); return callback;
                          }
                          console.log("Klaar met ordertickets");

                          switch (obj["ticket"]) {
                              case 0:
                                  conn.query('UPDATE "TicketSales" SET "Friday" = "Friday" - 1;', function (err, result) {

                                      //disconnect after successful commit
                                      if (err) {
                                          console.log(' ==== O141 ===' + err);
                                          rollback(conn); return callback;
                                      }
                                  });
                                  break;
                              case 1:
                                  conn.query('UPDATE "TicketSales" SET "Saturday" = "Saturday" - 1;', function (err, result) {

                                      //disconnect after successful commit
                                      if (err) {
                                          console.log(' ==== O141 ===' + err);
                                          rollback(conn); return callback;
                                      }
                                  });
                                  break;
                              case 2:

                                  conn.query('UPDATE "TicketSales" SET "Sunday" = "Sunday" - 1;', function (err, result) {

                                      //disconnect after successful commit
                                      if (err) {
                                          console.log(' ==== O141 ===' + err);
                                          rollback(conn); return callback;
                                      }
                                  });
                                  break;

                          }
                          //Tickets verlagen voor de gekozen dagen
                          console.log(JSON.stringify(QRdata));
                          console.log("Tickets verlaagd");
                          function stringToAscii(s) {
                              var ascii = "";
                              if (s.length > 0)
                                  for (var i = 0; i < s.length; i++) {
                                      var c = "" + s.charCodeAt(i);
                                      while (c.length < 3)
                                          c = "0" + c;
                                      ascii += c;
                                  }
                              return (ascii);
                          }


                          var OrderPDF = new pdfkit;


                          OrderPDF.pipe(fs.createWriteStream(pdfName));


                          for (let pageCount = 0; pageCount < QRdata.length; pageCount++) {


                              //Elke keer draaien als er een barcode moet worden geplaatst
                              OrderPDF
                                  .font('Times-Roman')
                                  .fontSize(25)
                                  .text(QRdata[pageCount].pdfstring, 100, 80);

                              OrderPDF.scale(10)
                                  .translate(10, 15)
                                  .path(QRdata[pageCount].svg)
                                  .fill('blue', 'even-odd')
                                  .restore();

                          }
                          console.log('klaar');
                          OrderPDF.end();



                      });


                  });




              function sendPDF() {
                  User.getUserEmailByID(obj["idUser"], function (err, result) {

                      if (err) {
                          console.log(err);

                      } else {
                          let UserMail = result;

                          var helper = require('sendgrid').mail;
                          var mail = new helper.Mail();


                          var attachment = new helper.Attachment();
                          var file = fs.readFileSync(pdfName);
                          var base64File = new Buffer(file).toString('base64');
                          attachment.setContent(base64File);
                          attachment.setType('application/tpdf');
                          attachment.setFilename(pdfName);
                          attachment.setDisposition('attachment');
                          mail.addAttachment(attachment);



                          var email = new helper.Email(UserMail, 'Example User');
                          mail.setFrom(email);

                          mail.setSubject('Uw kaarten voor conferentie ICT');

                          var personalization = new helper.Personalization();
                          email = new helper.Email(UserMail, 'Example User');
                          personalization.addTo(email);
                          mail.addPersonalization(personalization);

                          var content = new helper.Content('text/html', 'In de bijlage kan u uw vrijkaarten vinden voor de conferentie')
                          mail.addContent(content);


                          var sg = require('sendgrid')(config.sengridAPI);
                          var request = sg.emptyRequest({
                              method: 'POST',
                              path: '/v3/mail/send',
                              body: mail.toJSON()
                          });

                          sg.API(request, function (error, response) {
                              console.log(response.statusCode);
                              console.log(response.body);
                              console.log(response.headers);
                          });
                      }


                  });
              } setTimeout(sendPDF, 3000);






          });

          //Commit transactie

          console.log("commit uitgevoerd");
          conn.query('COMMIT');
          return callback(null, ido);
      });

  });




}

Timeslot.DenyRequest = function (idTimeslot, callback) {
    conn.query('DELETE FROM "Timeslot" WHERE "idTimeslot" = $1 ;', [idTimeslot], function (err, result) {
        if (err) {
            console.log("==== Deny Request ==== " + err);
            return callback(err, null);
        } else {

            return callback(null, result);
        }

    });

}

Timeslot.SecondaryChoice = function (idTimeslot, callback) {
    conn.query('UPDATE "Timeslot" SET "reviewed" = TRUE, "idSlot" = "prefferedSlot" where "idTimeslot" = $1 RETURNING "prefferedSlot";', [idTimeslot], function (err, result) {
        if (err) {
            console.log("==== Secondary Choice Request ==== " + err);
            return callback(err, null);
        } else {
          console.log("timeslot is " + idTimeslot);
          conn.query('SELECT * FROM "Timeslot" where "idTimeslot" = $1', [idTimeslot], function(err, idslot){
            if(err){
              console.log(err)
            }else{
              console.log(JSON.stringify(idslot));
          Timeslot.GetUserIDbyTimeslot(idTimeslot, function(err, idUser){
            if(err){
              console.log(err);
            }else{
              Timeslot.ToDateConvert(parseInt(result.rows[0].prefferedSlot), function(err, result){
                if(err){
                  console.log(err);
                }else{
                  console.log(JSON.stringify(result));
                  var obj = {
                    idUser : idUser,
                    ticket : parseInt(result)
                  }
                  Timeslot.generatePDFAndSendMail(obj, function(err, result){
                    if(err){
                      console.log("======================== PDF GEN ERR ==== " + err);
                    }else{
                      return callback(null, true);
                    }

                  });

                }

              })

            }


          });
                }

            })

        }

    });

}

Timeslot.OpenSlots = function(callback){
  conn.query('Select "SlotTimes"."idSlot" , "slotValue" from "SlotTimes", "Timeslot" where "Timeslot"."reviewed" = False and  "Timeslot"."idSlot" != "SlotTimes"."idSlot"  GROUP BY "SlotTimes"."idSlot", "slotValue" Order BY "idSlot" ASC;', function(err, result){
    if(err){
      console.log('=== OpenSlots === ' + err);
      return callback(err, null);
    }else {
      if(result.rows[0] == null || typeof(result.rows[0]) == 'undefined'){
        conn.query('Select "SlotTimes"."idSlot" , "slotValue" from "SlotTimes" Order BY "idSlot" ASC', function(err, results){
          if(err){
            console.log('=== OpenSlots === ' + err);
            return callback(err, null);
          }else{

        return callback(null, results.rows);
      }
      });
    }
      console.log(result.rows + " is de OpenSlots uitkomst");
      return callback(null, result.rows);
    }
  });

}
Timeslot.getSlot = function(idTimeslot, callback){
  conn.query('SELECT * FROM "Timeslot"  where "idTimeslot" = $1', [idTimeslot], function(err, result){
    if(err){
      console.log('=== GET SLOTS == ' + err);
      return callback(err, null);
    }else{
      return callback(null, result);
    }

  });

}
Timeslot.ToDateConvert = function (idSlot, callback) {

    Number.prototype.between = function (a, b, inclusive) {
        var min = Math.min(a, b),
            max = Math.max(a, b);
        return inclusive ? this >= min && this <= max : this > min && this < max;
    }

    if (idSlot.between(1, 20, true)) {
        var DayDate = "0";
        return callback(null, DayDate);

    } else if (idSlot.between(21, 56, true)) {
        var DayDate = "1";
        return callback(null, DayDate);

    } else if (idSlot.between(57, 72, true)) {
        var DayDate = "2";
        return callback(null, DayDate);

    } else {
        return callback("no date found");
    }

}

Timeslot.GetUserIDbyTimeslot = function(idTimeslot, callback){

  conn.query('SELECT "idUser" FROM "userTimeslot" WHERE "idTimeslot" = $1', [idTimeslot], function(err, result){
      if(err){
        console.log("Get User By Id  ====== " +  err);
        callback(err, null);
      }else{
        console.log(" aa " + JSON.stringify(result));
        return callback(null, result.rows[0].idUser);
      }
  });
}


module.exports = Timeslot;
