'use strict';
const pg = require('pg');
const config = require('../dbConn/config.js');
var sendgrid = require('sendgrid')(config.sendgridAPI);
var Client = require('pg').Client;
var _ = require('underscore');
var moment = require('moment');



var conn = new Client(config.conn);
conn.connect();

var rollback = function (conn) {
    //terminating a conn connection will
    //automatically rollback any uncommitted transactions
    //so while it's not technically mandatory to call
    //ROLLBACK it is cleaner and more correct
    conn.query('ROLLBACK', function () {

    });
};

var Tags = function () {

};

Tags.NewTags = function (obj, callback) {

    conn.query('BEGIN', function (err, result) {
        if (err) { console.log(err); rollback(conn); return callback(err, null); }

        //New OrderRequest
        conn.query('', [], function (err, result) {
            if (err) {
                console.log(err);
                rollback(conn);
                return callback(err, null);
            } else {

            }



        });

    });
  return callback(null, TRUE);
    //Assigning request to specific User
}

Tags.TimeslotTag = function(obj, callback){
  conn.query('BEGIN', function (err, result) {
      if (err) { console.log(err); rollback(conn); return callback(err, null); }


      for(let a = 0; a < Object.keys(obj).length ; a++){
        console.log("timeslot id = " + obj["timeslot"].idTimeslot);
        console.log(JSON.stringify(obj["tags"][a]));
      //New OrderRequest
      console.log(a);
      conn.query('INSERT INTO "timeslotTag" ("idtimeslotTag", "idTag") VALUES ($1, $2);', [obj["timeslot"].idTimeslot, obj["tags"][a]], function (err, result) {
          if (err) {
              console.log(err);
              rollback(conn);
              return callback(err, null);
          }

      });
    }
    conn.query('COMMIT');
    return callback(null, true);
  });
}

Tags.getTags = function(callback){
  conn.query('SELECT * FROM "Tag";', function(err, result){
    if(err){
      console.log("=====get tags ===" + err);
      return callback(err, null);
    }else{
      console.log(result.rows);
      return callback(null, result.rows);
    }

  });

}


module.exports = Tags;
