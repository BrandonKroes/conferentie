﻿'use strict'

const pg = require('pg')
const config = require('../dbConn/config.js');
var sendgrid = require('sendgrid')(config.sendgridAPI);
var Client = require('pg').Client;
var _ = require('underscore');

var conn = new Client(config.conn);
conn.connect();

var rollback = function (conn) {
    //terminating a conn connection will
    //automatically rollback any uncommitted transactions
    //so while it's not technically mandatory to call
    //ROLLBACK it is cleaner and more correct
    conn.query('ROLLBACK', function () {
        conn.end();
    });
};



var moment = require('moment');
console.log(moment().format("YYYY[/]MM[/]DD"));

var User = function () {
    gatheredID = null,
        idUSer = null,
        email = "querytest21@mail.nl",
        firstname = "querytestfirstname",
        infix = "querytestinfix",
        lastname = "querytestlastname",
        profilepicture = "querytestprofilepicture",
        birthday = "2013-03-11",
        street = "queryteststreet",
        city = "querytestcity",
        province = "querytestprovince",
        country = "querytestcountry",
        phone = "querytestphone",
        mobilephone = "querytestmobilephone",
        postalcode = "querytestpostalcode",
        billingadres = "querytestbillingadres",
        streetbilling = "queryteststreetbilling",
        citybilling = "querytestcitybilling",
        provincebilling = "querytestprovincebilling",
        countrybilling = "querytestcountrybilling",
        password = "querypassword"

};


//creating a new user
User.insertNewUser = function (obj, callback) {

    function randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
        return result;
    }
    let randomLink = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');


    conn.query('BEGIN', function (err, result) {
        if (err) return rollback(conn);
        conn.query('INSERT INTO "User" ("name", "streetname", "city", "zipcode", "country", "province") VALUES ($1, $2, $3, $4, $5, $6) RETURNING "idUser";', [obj.name, obj.streetname, obj.city, obj.zipcode, obj.country, obj.province], function (err, result) {
            if (err) {
                console.log(' ==== UI1 ===' + err);
                return rollback(conn);
            }
            let idu = result.rows[0].idUser;

            conn.query('INSERT INTO "userRole" ("idUser", "idRole") VALUES ($1, 2); ', [idu], function (err, result) {
                if (err) {
                    console.log(' ==== UI2 ===' + err);
                    return rollback(conn);
                }
                //disconnect after successful commit

                conn.query('INSERT INTO "Login" ("idUser", "password", "email", "activation", "activated") VALUES ($1, $2, $3, $4, $5);', [idu, obj.password, obj.email, randomLink, false], function (err, result) {
                    if (err) {
                        console.log(' ==== UI3 ===' + err);
                        return rollback(conn);
                    }
                    //disconnect after successful commit
                    console.log('link is ' + randomLink);
                    conn.query('COMMIT', conn.end.bind(conn));
                    conn.end();
                    return callback(null, randomLink);
                });
            });
        });
    });
}

User.checkExistingEmail = function (obj, callback) {
    conn.query('select count(*) from "Login" where "email" = $1;', [obj.email], function (err, result) {
        if (err) return conn.end();
        let count = parseInt(result.rows[0].count);
        return callback(null, count);
    });
}

User.activationURL = function (hashUrl, callback) {
    conn.query('update "Login" SET activated = true WHERE activation  = $1;', [hashUrl], function (err, result) {
        if (err) {
            return callback(null, false);
        } else {
            return callback(null, true);
        }
    });

}

User.login = function (obj, callback) {
    conn.query('select "idUser" from "Login" where "email"=$1 and "password"=$2 and activated = true;', [obj.email, obj.password], function (err, result) {
        if (err) {
            console.log(err);
            conn.end();
            return callback(null, false);
        } else {
            let check = _.isEmpty(result.rows[0]);
            console.log(check + " check ");
            if (check === true) {
                return callback(null, false);
            }

            else {
                console.log(JSON.stringify(result.rows[0]));
                let idUser = result.rows[0].idUser;
                conn.query('select "idUser", "idRole" from "userRole" where "idUser"=$1;', [idUser], function (err, result) {
                    if (err) {
                        console.log(err + "130");
                        return callback(null, false);

                    } else {
                        var str = JSON.stringify(result, null, 4); // (Optional) beautiful indented output.
                        console.log(str); // Logs output to dev tools console.
                        return callback(null, result);
                    }
                });
            }

        }
    });
}

User.getUserEmailByID = function (id, callback) {
    conn.query('Select "email" from "Login" where "idUser" = $1;', [id], function (err, result) {
        if (err) {
            console.log(err);
            conn.end();
            return callback(null, false);
        } else {
            let UserEmail = result.rows[0].email;
            console.log(UserEmail);
            return callback(null, UserEmail);
        }
    });
}

User.HowManyIn = function(callback){

  conn.query('select count(*) as TicketIn from "Ticket" where "ticketIn" = true;', function(err, result){
    if(err){
      console.log(err);
      return callback(null, false);
    }
    else{
      console.log(JSON.stringify(result.rows[0].ticketin));
      return callback(null, result.rows[0].ticketin );
    }

  });

}


module.exports = User;
