﻿--	DROP SCHEMA public C ADE;
--	CREATE SCHEMA public;
--	COMMENT ON SCHEMA public IS 'standard public schema';

	DROP TABLE IF EXISTS "User" ;
	CREATE  TABLE IF NOT EXISTS "User" (
	  "idUser" SERIAL NOT NULL  ,
	  "name" VARCHAR(45) NOT NULL ,
	  "streetname" VARCHAR(45) NOT NULL ,
	  "city" VARCHAR(45) NOT NULL ,
	  "zipcode" VARCHAR(45) NOT NULL ,
	  "country" VARCHAR(45) NOT NULL ,
	  "province" VARCHAR(45) NOT NULL ,
	  PRIMARY KEY ("idUser") )
	;


	-- -----------------------------------------------------
	-- Table "Login"
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS "Login" ;

	CREATE  TABLE IF NOT EXISTS "Login" (
	  "idUser" INT NOT NULL ,
	  "password" VARCHAR(45) NOT NULL ,
	  "email" VARCHAR(45) NOT NULL ,
	  "activation" VARCHAR(45) NOT NULL ,
	  "activated" BOOLEAN NOT NULL ,
	  PRIMARY KEY ("idUser"));


	-- -----------------------------------------------------
	-- Table "Role"
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS "Role" ;

	CREATE  TABLE IF NOT EXISTS "Role" (
	  "idRole" SERIAL NOT NULL ,
	  "description" VARCHAR(45) NOT NULL ,
	  PRIMARY KEY ("idRole") )
	;


	-- -----------------------------------------------------
	-- Table "userRole"
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS "userRole" ;

	CREATE  TABLE IF NOT EXISTS "userRole" (
	  "idUser" INT NOT NULL ,
	  "idRole" INT NOT NULL ,
	  PRIMARY KEY ("idUser") ,
	  CONSTRAINT "fk_Role"
		FOREIGN KEY ("idRole" )
		REFERENCES "Role" ("idRole" )
		ON DELETE NO ACTION
		ON UPDATE NO ACTION);


	INSERT INTO "Role" ("idRole", "description") VALUES (1, 'Customer');
	INSERT INTO "Role" ("idRole", "description") VALUES (2, 'Admin');
	INSERT INTO "Login" ("idUser", "password", "email", "activation", "activated") VALUES (1, 'brandon', 'ctscriptwrite@gmail.com', 'aaaabbb', TRUE);




	-- -----------------------------------------------------

	-- Table "Order"

	-- -----------------------------------------------------

	CREATE  TABLE IF NOT EXISTS "Order" (
	  "idOrder" VARCHAR(45) NOT NULL ,
	  "orderDate" TIMESTAMP NOT NULL ,
	  "orderPrice" FLOAT NOT NULL ,
	  "orderState" BOOLEAN NOT NULL ,
	  "orderPaid" BOOLEAN NOT NULL ,
	  PRIMARY KEY ("idOrder") );





	-- -----------------------------------------------------

	-- Table "Ticket"

	-- -----------------------------------------------------

	CREATE  TABLE IF NOT EXISTS "Ticket" (
	  "idTicket" SERIAL NOT NULL ,
	  "ticketIn" BOOLEAN NOT NULL ,
	  "ticketDay" SMALLINT NOT NULL ,
	  "barcode" VARCHAR(45) NOT NULL ,
	  PRIMARY KEY ("idTicket") )
	;





	-- -----------------------------------------------------

	-- Table "orderTickets"

	-- -----------------------------------------------------

	CREATE  TABLE IF NOT EXISTS "orderTickets" (
	  "idTicket" INT NOT NULL ,
	  "idOrder" VARCHAR(45) NOT NULL ,
	  PRIMARY KEY ("idTicket")
	  );




	-- -----------------------------------------------------

	-- Table "userOrders"

	-- -----------------------------------------------------

	CREATE  TABLE IF NOT EXISTS "userOrders" (

	  "idUser" INT NOT NULL ,
	  "idOrder" VARCHAR(45) NOT NULL ,
	  PRIMARY KEY ("idOrder"));




	-- -----------------------------------------------------

	-- Table "Meal"

	-- -----------------------------------------------------

	CREATE  TABLE IF NOT EXISTS "Meal" (

	  "idMeal" SERIAL ,
	  "foodType" BOOLEAN NOT NULL ,
	  "barcode" VARCHAR(45) NOT NULL ,
	  "foodtime" BOOLEAN NOT NULL ,
	  "mealDate" SMALLINT NOT NULL ,
	  "used" BOOLEAN NOT NULL ,
	  PRIMARY KEY ("idMeal") );





	-- -----------------------------------------------------

	-- Table "orderMeals"

	-- -----------------------------------------------------

	CREATE  TABLE IF NOT EXISTS "orderMeals" (

	  "idMeals" INT NOT NULL ,
	"idOrders" VARCHAR(45) NOT NULL ,
	 PRIMARY KEY ("idMeals"));
	  ALTER TABLE "orderMeals"
	   ADD FOREIGN KEY ("idOrders") REFERENCES "Order" ("idOrder");
	 ALTER TABLE "orderMeals"
	   ADD FOREIGN KEY ("idMeals") REFERENCES "Meal" ("idMeal");

	   	   ALTER TABLE "orderMeals"
	   ADD FOREIGN KEY ("idOrders") REFERENCES "Order" ("idOrder");
	 ALTER TABLE "orderMeals"
	   ADD FOREIGN KEY ("idMeals") REFERENCES "Meal" ("idMeal");


	   CREATE  TABLE IF NOT EXISTS "userOrders" (

	  "idUser" INT NOT NULL ,
	  "idOrder" VARCHAR(45) NOT NULL ,
	  PRIMARY KEY ("idOrder"));




	CREATE  TABLE IF NOT EXISTS "orderTickets" (
	  "idTicket" INT NOT NULL ,
	  "idOrder" VARCHAR(45) NOT NULL ,
	  PRIMARY KEY ("idTicket")
	  );

	 ALTER TABLE "orderTickets"
	   ADD FOREIGN KEY ("idOrder") REFERENCES "Order" ("idOrder");
	 ALTER TABLE "orderTickets"
	   ADD FOREIGN KEY ("idTicket") REFERENCES "Ticket" ("idTicket");

	-- -----------------------------------------------------
	-- Table "TicketSales"
	-- -----------------------------------------------------
	CREATE  TABLE IF NOT EXISTS "TicketSales" (
	  "Friday" INT NOT NULL ,
	  "Saturday" INT NOT NULL ,
	  "Sunday" INT NOT NULL );


-- -----------------------------------------------------
-- Table "Tag"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "Tag" ;

CREATE  TABLE IF NOT EXISTS "Tag" (
  "idTag" SERIAL NOT NULL ,
  "Tag" VARCHAR(45) NOT NULL ,
  PRIMARY KEY ("idTag") )
;


-- -----------------------------------------------------
-- Table "Timeslot"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "Timeslot" ;

CREATE  TABLE IF NOT EXISTS "Timeslot" (
  "idTimeslot" SERIAL NOT NULL ,
  "idSlot" INT NOT NULL ,
  "topic" VARCHAR(45) NOT NULL ,
  "wishes" VARCHAR(45) NOT NULL ,
  "prefferedSlot" INT NOT NULL ,
  "description" VARCHAR(45) NOT NULL ,
  "reviewed" BOOLEAN NOT NULL ,
  PRIMARY KEY ("idTimeslot") )
;


-- -----------------------------------------------------
-- Table "timeslotTag"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "timeslotTag" ;

CREATE  TABLE IF NOT EXISTS "timeslotTag" (
  "idtimeslotTag" INT NOT NULL ,
  "idTag" INT NOT NULL );



-- -----------------------------------------------------
-- Table "userTimeslot"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "userTimeslot" ;

CREATE  TABLE IF NOT EXISTS "userTimeslot" (
  "idTimeslot" INT NOT NULL ,
  "idUser" INT NOT NULL ,
  PRIMARY KEY ("idTimeslot"));

CREATE TABLE "SlotTimes"("idSlot" SERIAL NOT NULL, "slotValue" varchar(45) NOT NULL);
INSERT INTO "SlotTimes" ("idSlot", "slotValue") VALUES (1, 'Customer');
